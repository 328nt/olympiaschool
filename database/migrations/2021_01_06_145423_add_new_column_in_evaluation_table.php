<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnInEvaluationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->integer('card_id')->unsigned()->nullable();
            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');
            $table->integer('card_option_id')->unsigned()->nullable();
            $table->foreign('card_option_id')->references('id')->on('card_options')->onDelete('cascade');
            $table->longText('reason')->nullable()->default('text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            //
        });
    }
}
