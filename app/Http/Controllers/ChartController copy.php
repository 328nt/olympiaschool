<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evaluation;
use App\Schedule;
use App\Address;
use App\Criteria;
use App\Subject;
use App\Teacher;
use App\Obs_type;
use App\Card_options;
use App\User_teacher;
use Auth;
use Mail;

class ChartController extends Controller

{
    public function chart_criteria()
    {
        $criterias = Criteria::all();
        return view('be.charts.select_criteria', compact('criterias'));
    }
    
    
    public function chart_by_grade(Request $rq, $id)
    {
        // $id = $rq->criterias;
        $evaluations = Evaluation::all();
        $date_start = "2021-01-01 00:00:01";
        $date_end = "2021-01-31 23:59:01";
        $grades = Evaluation::where('status', 1)
        ->groupBy('grade')
        ->select('grade')
        ->get();
        // dd($grades);
        echo $grades .'<br>';
        if ($grades->isEmpty()) {
            return "fail";
        }
        
        $evalu_by_sub = Evaluation::where('status', 1)
        ->whereBetween('created_at', [ $date_start, $date_end ] )
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')
        ->get();
        dd($evalu_by_sub);

        
        if (count($evalu_by_sub) == 0) {
            $by_criteria_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $by_criteria_devel[] = array('label' => "developing", 'y' => 0);
            $by_criteria_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $by_criteria_outst[] = array('label' => "Outstanding", 'y' => 0);
            $by_criteria_0[] = array('label' => $criteria, 'y' => 0);
            $new_total[] = array('label' => "Cap 1", 'y' => 0);
        } else {
            foreach ($evalu_by_sub as $evalu_by_teacher) {
                // dd($evalu_by_teacher);
                $med_for_teacher = Evaluation::where('status', 1)
                ->whereBetween('created_at', [ $date_start, $date_end ] )
                // ->where('grade', 2)
                // ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                ->get();
                // echo $med_for_teacher .'<br>';
                // dd(($med_for_teacher));
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            $total = $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
                                    + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    + $total_p5;
                            $tb = $total/count($med_for_teacher);
                            // dd($total);
                }
                $json_tb_l3 = json_encode($tb);
                $test_total[] = json_decode($tb);
                $med_total= array_sum($test_total);
                $chart_total_total[] = array('label' => $evalu_by_grade->teacher->fullname, 'y' => $tb);
            }
            $qqq = ($med_total/count($test_total));
            $in_total_total = 0;
            $de_total_total = 0;
            $ac_total_total = 0;
            $ou_total_total = 0;
            // dd($chart_total_total);
            foreach ($chart_total_total as $value) {
                // dd($value['y']);
                if ($value['y'] < $total_point*0.3) {
                    $in_total_total++;
                }
                if ($value['y'] >= $total_point*0.3 && $value['y'] < $total_point*0.6) {
                    $de_total_total++;
                }
                if ($value['y'] >= $total_point*0.6 && $value['y'] < $total_point*0.7) {
                    $ac_total_total++;
                }
                if ($value['y'] >= $total_point*0.7) {
                    $ou_total_total++;
                }
            }
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => $in_total_total);
            $chart_total_devel[] = array('label' => "developing", 'y' => $de_total_total);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => $ac_total_total);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => $ou_total_total);
            $chart_total_0[] = array('label' => "Total", 'y' => 0);
        }
        
        // foreach ($grades as $grade) {
        //     $evaluation_by_grade = Evaluation::where('status', 1)
        //     ->where('grade',$grade->grade)
        //     ->get();
        //     foreach ($evaluation_by_grade as $evalu_by_grade) {
        //         $p1a1 = $p1a2 = $p1a3 = 0;
        //         $p1b1 = $p1b2 = $p1c = 0;
        //         $p2a1 = $p2a2 = $p2a3 = 0;
        //         $p2b1 = $p2b2 = $p2c = 0;
        //         $p2c1 = $p2c2 = $p2c3 = 0;
        //         $p2d1 = $p2d2 = 0;
        //         $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
        //         $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
        //         $p3d1 = $p3d2 = $p3e1 = 0;
        //         $p4a = $p4b = $p4c = 0;
        //         $p4d1 = $p4d2 = 0;
        //         $p5a = $p5b = $p5c = 0;
        //         $med = 0;
        //         $basic_point = 0;
        //         $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
        //         $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
        //         $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
        //         $total_p1a = ($p1a1+$p1a2 + $p1a3);
        //         $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
        //         $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
        //         $total_p1b = ($p1b1+$p1b2);
        //         $p1c+= array_sum($evalu_by_grade->part1['p1c']);
        //         $total_p1c = ($p1c);
        //         // dd($total_p1);
        //         $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
        //         $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
        //         $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
        //         $total_p2a = ($p2a1+$p2a2+$p2a3);
        //         $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
        //         $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
        //         $total_p2b = ($p2b1+$p2b2);
        //         $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
        //         $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
        //         $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
        //         $total_p2c = ($p2c1+$p2c2+$p2c3);
        //         $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
        //         $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
        //         $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
        //         $total_p3a = ($p3a1+$p3a2+$p3a3);
        //         $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
        //         $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
        //         $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
        //         $total_p3b = ($p3b1+$p3b2+$p3b3);
        //         $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
        //         $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
        //         $total_p3c = ($p3c1+$p3c2);
        //         $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
        //         $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
        //         $total_p3d = ($p3d1+$p3d2);
        //         $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
        //         $total_p3e = ($p3e1);
        //         $p4a+= array_sum($evalu_by_grade->part4['p4a']);
        //         $total_p4a = $p4a;
        //         $p4b+= array_sum($evalu_by_grade->part4['p4b']);
        //         $total_p4b = $p4b;
        //         $p4c+= array_sum($evalu_by_grade->part4['p4c']);
        //         $total_p4c = $p4c;
        //         $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
        //         $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
        //         $total_p4d = ($p4d1+$p4d2);
        //         $p5a+= array_sum($evalu_by_grade->part5['p5a']);
        //         $p5b+= array_sum($evalu_by_grade->part5['p5b']);
        //         $p5c+= array_sum($evalu_by_grade->part5['p5c']);
        //         $total_p5 = ($p5a+$p5b+$p5c);
        //         // dd($total_p5);
        //         $total = $total_p1a + $total_p1b + $total_p1c
        //                 + $total_p2a + $total_p2b + $total_p2c 
        //                 + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
        //                 + $total_p4a + $total_p4b + $total_p4c + $total_p4d
        //                 + $total_p5;
        //                 $tb = $total/count($evaluation_by_grade);
        //                 // dd($tb);
        //     }
        //     $new_evaluations[] = array('label' => $evalu_by_grade->id, 'y' => $tb);
        // }
        // dd($new_evaluations);
        
        $evaluations_c1 = Evaluation::where('status', 1)
        ->where('grade',3)
        // ->where('criteria_id', 3)
        ->groupBy('id_teacher')
        ->select('id_teacher')->get();
        // dd(($evaluations_c1));

        foreach ($evaluations_c1 as $evalu_by_teacher) {
            $med_for_teacher = Evaluation::where('status', 1)
            ->where('grade',3)
            // ->where('criteria_id', 3)
            ->where('id_teacher', $evalu_by_teacher->id_teacher)
            // ->groupBy('part1', 'part2a')
            // ->select('part1', 'part2a')
            ->get();
            // echo $med_for_teacher .'<br>';
            // dd(($med_for_teacher));
                $p1a1 = $p1a2 = $p1a3 = 0;
                $p1b1 = $p1b2 = $p1c = 0;
                $p2a1 = $p2a2 = $p2a3 = 0;
                $p2b1 = $p2b2 = $p2c = 0;
                $p2c1 = $p2c2 = $p2c3 = 0;
                $p2d1 = $p2d2 = 0;
                $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                $p3d1 = $p3d2 = $p3e1 = 0;
                $p4a = $p4b = $p4c = 0;
                $p4d1 = $p4d2 = 0;
                $p5a = $p5b = $p5c = 0;
                $med = 0;
                $basic_point = 0;
            foreach ($med_for_teacher as $evalu_by_grade) {
                        $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                        $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                        $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                        $total_p1a = ($p1a1+$p1a2 + $p1a3);
                        $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                        $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                        $total_p1b = ($p1b1+$p1b2);
                        $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                        $total_p1c = ($p1c);
                        // dd($total_p1);
                        $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                        $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                        $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                        $total_p2a = ($p2a1+$p2a2+$p2a3);
                        $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                        $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                        $total_p2b = ($p2b1+$p2b2);
                        $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                        $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                        $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                        $total_p2c = ($p2c1+$p2c2+$p2c3);
                        $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                        $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                        $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                        $total_p3a = ($p3a1+$p3a2+$p3a3);
                        $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                        $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                        $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                        $total_p3b = ($p3b1+$p3b2+$p3b3);
                        $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                        $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                        $total_p3c = ($p3c1+$p3c2);
                        $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                        $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                        $total_p3d = ($p3d1+$p3d2);
                        $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                        $total_p3e = ($p3e1);
                        $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                        $total_p4a = $p4a;
                        $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                        $total_p4b = $p4b;
                        $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                        $total_p4c = $p4c;
                        $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                        $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                        $total_p4d = ($p4d1+$p4d2);
                        $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                        $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                        $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                        $total_p5 = ($p5a+$p5b+$p5c);
                        // dd($total_p5);
                        $total = $total_p1a + $total_p1b + $total_p1c
                                + $total_p2a + $total_p2b + $total_p2c 
                                + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
                                + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                + $total_p5;
                        $tb = $total/count($med_for_teacher);
            }
            $json_tb_l3 = json_encode($tb);
            $test_g2[] = json_decode($tb);
            $med_g2= array_sum($test_g2);
            $chart_teacher_c1[] = array('label' => $evalu_by_grade->teacher->fullname, 'y' => $tb);
            // break;
        }
        $qqq = ($med_g2/count($test_g2));
        // dd($qqq);


        
            $criteria = Criteria::find($id)->name;
        if ($id == 1 || $id == 2) {
            $total_point = 365;
        } elseif($id == 3) {
            $total_point = 355;
        } elseif($id == 4) {
            $total_point = 358;
        } elseif($id == 5) {
            $total_point = 360;
        }




        $evaluations_total = Evaluation::where('status', 1)
        // ->where('grade', 2)
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')->get();
        // dd(($evaluations_total));
        if (count($evaluations_total) == 0) {
            $chart_teacher_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_teacher_devel[] = array('label' => "developing", 'y' => 0);
            $chart_teacher_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_teacher_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_teacher_0[] = array('label' => "Total", 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => 0);
        } else {
            foreach ($evaluations_total as $evalu_by_teacher) {
                $med_for_teacher = Evaluation::where('status', 1)
                // ->where('grade', 2)
                ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                // ->groupBy('part1', 'part2a')
                // ->select('part1', 'part2a')
                ->get();
                // echo $med_for_teacher .'<br>';
                // dd(($med_for_teacher));
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            // dd($total_p1);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            // dd($total_p5);
                            $total = $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
                                    + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    + $total_p5;
                            $tb = $total/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_total[] = json_decode($tb);
                $med_total= array_sum($test_total);
                $chart_teacher_total[] = array('label' => $evalu_by_grade->teacher->fullname, 'y' => $tb);
            }
            $qqq = ($med_total/count($test_total));
            $in_total = 0;
            $de_total = 0;
            $ac_total = 0;
            $ou_total = 0;
            // dd($total_point);
            foreach ($chart_teacher_total as $value) {
                // dd($value['y']);
                if ($value['y'] < $total_point*0.3) {
                    $in_total++;
                }
                if ($value['y'] >= $total_point*0.3 && $value['y'] < $total_point*0.6) {
                    $de_total++;
                }
                if ($value['y'] >= $total_point*0.6 && $value['y'] < $total_point*0.7) {
                    $ac_total++;
                }
                if ($value['y'] >= $total_point*0.7) {
                    $ou_total++;
                }
            }
            $chart_teacher_ineff[] = array('label' => "Ineffective", 'y' => $in_total);
            $chart_teacher_devel[] = array('label' => "developing", 'y' => $de_total);
            $chart_teacher_accom[] = array('label' => "Accomplishing", 'y' => $ac_total);
            $chart_teacher_outst[] = array('label' => "Outstanding", 'y' => $ou_total);
            $chart_teacher_0[] = array('label' => "Total", 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => $qqq);
            // dd($new_evaluations);
        }
        
        $evaluations_mn = Evaluation::where('status', 1)
        ->where('grade', 1)
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')->get();
        // dd(($evaluations_mn));
        if (count($evaluations_mn) == 0) {
            $chart_teacher_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_teacher_devel[] = array('label' => "developing", 'y' => 0);
            $chart_teacher_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_teacher_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_teacher_0[] = array('label' => "Kindergarten", 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => 0);
        } else {
            foreach ($evaluations_mn as $evalu_by_teacher) {
                $med_for_teacher = Evaluation::where('status', 1)
                ->where('grade', 2)
                ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                // ->groupBy('part1', 'part2a')
                // ->select('part1', 'part2a')
                ->get();
                // echo $med_for_teacher .'<br>';
                // dd(($med_for_teacher));
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            // dd($total_p1);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            // dd($total_p5);
                            $total = $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
                                    + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    + $total_p5;
                            $tb = $total/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_mn[] = json_decode($tb);
                $med_mn= array_sum($test_mn);
                $chart_teacher_mn[] = array('label' => $evalu_by_grade->teacher->fullname, 'y' => $tb);
            }
            $qqq = ($med_mn/count($test_mn));
            $in_mn = 0;
            $de_mn = 0;
            $ac_mn = 0;
            $ou_mn = 0;
            // dd($total_point);
            foreach ($chart_teacher_mn as $value) {
                // dd($value['y']);
                if ($value['y'] < $total_point*0.3) {
                    $in_mn++;
                }
                if ($value['y'] >= $total_point*0.3 && $value['y'] < $total_point*0.6) {
                    $de_mn++;
                }
                if ($value['y'] >= $total_point*0.6 && $value['y'] < $total_point*0.7) {
                    $ac_mn++;
                }
                if ($value['y'] >= $total_point*0.7) {
                    $ou_mn++;
                }
            }
            $chart_teacher_ineff[] = array('label' => "Ineffective", 'y' => $in_mn);
            $chart_teacher_devel[] = array('label' => "developing", 'y' => $de_mn);
            $chart_teacher_accom[] = array('label' => "Accomplishing", 'y' => $ac_mn);
            $chart_teacher_outst[] = array('label' => "Outstanding", 'y' => $ou_mn);
            $chart_teacher_0[] = array('label' => "Kindergarten", 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => $qqq);
            // dd($new_evaluations);
        }

        $evaluations_c1 = Evaluation::where('status', 1)
        ->where('grade', 2)
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')->get();
        // dd(($evaluations_c1));
        if (count($evaluations_c1) == 0) {
            $chart_teacher_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_teacher_devel[] = array('label' => "developing", 'y' => 0);
            $chart_teacher_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_teacher_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_teacher_0[] = array('label' => "Primary", 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => 0);
        } else {
            foreach ($evaluations_c1 as $evalu_by_teacher) {
                $med_for_teacher = Evaluation::where('status', 1)
                ->where('grade', 2)
                ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                // ->groupBy('part1', 'part2a')
                // ->select('part1', 'part2a')
                ->get();
                // echo $med_for_teacher .'<br>';
                // dd(($med_for_teacher));
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            // dd($total_p1);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            // dd($total_p5);
                            $total = $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
                                    + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    + $total_p5;
                            $tb = $total/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_c1[] = json_decode($tb);
                $med_c1= array_sum($test_c1);
                $chart_teacher_c1[] = array('label' => $evalu_by_grade->teacher->fullname, 'y' => $tb);
            }
            $qqq = ($med_c1/count($test_c1));
            $in_c1 = 0;
            $de_c1 = 0;
            $ac_c1 = 0;
            $ou_c1 = 0;
            // dd($total_point);
            foreach ($chart_teacher_c1 as $value) {
                // dd($value['y']);
                if ($value['y'] < $total_point*0.3) {
                    $in_c1++;
                }
                if ($value['y'] >= $total_point*0.3 && $value['y'] < $total_point*0.6) {
                    $de_c1++;
                }
                if ($value['y'] >= $total_point*0.6 && $value['y'] < $total_point*0.7) {
                    $ac_c1++;
                }
                if ($value['y'] >= $total_point*0.7) {
                    $ou_c1++;
                }
            }
            $chart_teacher_ineff[] = array('label' => "Ineffective", 'y' => $in_c1);
            $chart_teacher_devel[] = array('label' => "developing", 'y' => $de_c1);
            $chart_teacher_accom[] = array('label' => "Accomplishing", 'y' => $ac_c1);
            $chart_teacher_outst[] = array('label' => "Outstanding", 'y' => $ou_c1);
            $chart_teacher_0[] = array('label' => "Primary", 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => $qqq);
            // dd($new_evaluations);
        }

        $evaluations_c2 = Evaluation::where('status', 1)
        ->where('grade', 3)
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')->get();
        // dd(($evaluations_c2));
        if (count($evaluations_c2) == 0) {
            $chart_teacher_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_teacher_devel[] = array('label' => "developing", 'y' => 0);
            $chart_teacher_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_teacher_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_teacher_0[] = array('label' => "Secondary", 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => 0);
        } else {
            foreach ($evaluations_c2 as $evalu_by_teacher) {
                $med_for_teacher = Evaluation::where('status', 1)
                ->where('grade', 3)
                ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                // ->groupBy('part1', 'part2a')
                // ->select('part1', 'part2a')
                ->get();
                // echo $med_for_teacher .'<br>';
                // dd(($med_for_teacher));
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            // dd($total_p1);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            // dd($total_p5);
                            $total = $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
                                    + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    + $total_p5;
                            $tb = $total/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_c2[] = json_decode($tb);
                $med_c2= array_sum($test_c2);
                $chart_teacher_c2[] = array('label' => $evalu_by_grade->teacher->fullname, 'y' => $tb);
            }
            $qqq = ($med_c2/count($test_c2));
            $in_c2 = 0;
            $de_c2 = 0;
            $ac_c2 = 0;
            $ou_c2 = 0;
            // dd($total_point);
            foreach ($chart_teacher_c2 as $value) {
                // dd($value['y']);
                if ($value['y'] < $total_point*0.3) {
                    $in_c2++;
                }
                if ($value['y'] >= $total_point*0.3 && $value['y'] < $total_point*0.6) {
                    $de_c2++;
                }
                if ($value['y'] >= $total_point*0.6 && $value['y'] < $total_point*0.7) {
                    $ac_c2++;
                }
                if ($value['y'] >= $total_point*0.7) {
                    $ou_c2++;
                }
            }
            $chart_teacher_ineff[] = array('label' => "Ineffective", 'y' => $in_c2);
            $chart_teacher_devel[] = array('label' => "developing", 'y' => $de_c2);
            $chart_teacher_accom[] = array('label' => "Accomplishing", 'y' => $ac_c2);
            $chart_teacher_outst[] = array('label' => "Outstanding", 'y' => $ou_c2);
            $chart_teacher_0[] = array('label' => "Secondary", 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => $qqq);
            // dd($new_evaluations);
        }

        
        $evaluations_c3 = Evaluation::where('status', 1)
        ->where('grade', 4)
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')->get();
        // dd(($evaluations_c3));
        if (count($evaluations_c3) == 0) {
            $chart_teacher_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_teacher_devel[] = array('label' => "developing", 'y' => 0);
            $chart_teacher_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_teacher_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_teacher_0[] = array('label' => "Highschool", 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => 0);
        } else {
            foreach ($evaluations_c3 as $evalu_by_teacher) {
                $med_for_teacher = Evaluation::where('status', 1)
                ->where('grade', 4)
                ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                // ->groupBy('part1', 'part2a')
                // ->select('part1', 'part2a')
                ->get();
                // echo $med_for_teacher .'<br>';
                // dd(($med_for_teacher));
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            // dd($total_p1);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            // dd($total_p5);
                            $total = $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
                                    + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    + $total_p5;
                            $tb = $total/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_c3[] = json_decode($tb);
                $med_c3= array_sum($test_c3);
                $chart_teacher_c3[] = array('label' => $evalu_by_grade->teacher->fullname, 'y' => $tb);
            }
            $qqq = ($med_c3/count($test_c3));
            $in_c3 = 0;
            $de_c3 = 0;
            $ac_c3 = 0;
            $ou_c3 = 0;
            // dd($total_point);
            foreach ($chart_teacher_c3 as $value) {
                // dd($value['y']);
                if ($value['y'] < $total_point*0.3) {
                    $in_c3++;
                }
                if ($value['y'] >= $total_point*0.3 && $value['y'] < $total_point*0.6) {
                    $de_c3++;
                }
                if ($value['y'] >= $total_point*0.6 && $value['y'] < $total_point*0.7) {
                    $ac_c3++;
                }
                if ($value['y'] >= $total_point*0.7) {
                    $ou_c3++;
                }
            }
            $chart_teacher_ineff[] = array('label' => "Ineffective", 'y' => $in_c3);
            $chart_teacher_devel[] = array('label' => "developing", 'y' => $de_c3);
            $chart_teacher_accom[] = array('label' => "Accomplishing", 'y' => $ac_c3);
            $chart_teacher_outst[] = array('label' => "Outstanding", 'y' => $ou_c3);
            $chart_teacher_0[] = array('label' => "Highschool", 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => $qqq);
            // dd($new_evaluations);
        }



        $evaluations_total = Evaluation::where('status', 1)
        // ->where('grade', 2)
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')->get();
        // dd(($evaluations_total));
        if (count($evaluations_total) == 0) {
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_total_devel[] = array('label' => "developing", 'y' => 0);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_total_0[] = array('label' => $criteria, 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => 0);
        } else {
            foreach ($evaluations_total as $evalu_by_teacher) {
                $med_for_teacher = Evaluation::where('status', 1)
                // ->where('grade', 2)
                ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                // ->groupBy('part1', 'part2a')
                // ->select('part1', 'part2a')
                ->get();
                // echo $med_for_teacher .'<br>';
                // dd(($med_for_teacher));
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            // dd($total_p1);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            // dd($total_p5);
                            $total = $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
                                    + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    + $total_p5;
                            $tb = $total/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_total[] = json_decode($tb);
                $med_total= array_sum($test_total);
                $chart_total_total[] = array('label' => $evalu_by_grade->teacher->fullname, 'y' => $tb);
            }
            $qqq = ($med_total/count($test_total));
            $in_total = 0;
            $de_total = 0;
            $ac_total = 0;
            $ou_total = 0;
            // dd($total_point);
            foreach ($chart_total_total as $value) {
                // dd($value['y']);
                if ($value['y'] < $total_point*0.3) {
                    $in_total++;
                }
                if ($value['y'] >= $total_point*0.3 && $value['y'] < $total_point*0.6) {
                    $de_total++;
                }
                if ($value['y'] >= $total_point*0.6 && $value['y'] < $total_point*0.7) {
                    $ac_total++;
                }
                if ($value['y'] >= $total_point*0.7) {
                    $ou_total++;
                }
            }
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => $in_total);
            $chart_total_devel[] = array('label' => "developing", 'y' => $de_total);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => $ac_total);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => $ou_total);
            $chart_total_0[] = array('label' => $criteria, 'y' => 0);
            $new_evaluations[] = array('label' => "Cap 1", 'y' => $qqq);
            // dd($new_evaluations);
        }



        $total_total = Evaluation::where('status', 1)
        // ->where('grade', 2)
        // ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')->get();
        // dd(($total_total));
        if (count($total_total) == 0) {
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_total_devel[] = array('label' => "developing", 'y' => 0);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_total_0[] = array('label' => $criteria, 'y' => 0);
            $new_total[] = array('label' => "Cap 1", 'y' => 0);
        } else {
            foreach ($total_total as $evalu_by_teacher) {
                $med_for_teacher = Evaluation::where('status', 1)
                // ->where('grade', 2)
                // ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                // ->groupBy('part1', 'part2a')
                // ->select('part1', 'part2a')
                ->get();
                // echo $med_for_teacher .'<br>';
                // dd(($med_for_teacher));
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            $total = $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
                                    + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    + $total_p5;
                            $tb = $total/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_total[] = json_decode($tb);
                $med_total= array_sum($test_total);
                $chart_total_total[] = array('label' => $evalu_by_grade->teacher->fullname, 'y' => $tb);
            }
            $qqq = ($med_total/count($test_total));
            $in_total_total = 0;
            $de_total_total = 0;
            $ac_total_total = 0;
            $ou_total_total = 0;
            // dd($total_point);
            foreach ($chart_total_total as $value) {
                // dd($value['y']);
                if ($value['y'] < $total_point*0.3) {
                    $in_total_total++;
                }
                if ($value['y'] >= $total_point*0.3 && $value['y'] < $total_point*0.6) {
                    $de_total_total++;
                }
                if ($value['y'] >= $total_point*0.6 && $value['y'] < $total_point*0.7) {
                    $ac_total_total++;
                }
                if ($value['y'] >= $total_point*0.7) {
                    $ou_total_total++;
                }
            }
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => $in_total_total);
            $chart_total_devel[] = array('label' => "developing", 'y' => $de_total_total);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => $ac_total_total);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => $ou_total_total);
            $chart_total_0[] = array('label' => "Total", 'y' => 0);
        }


        
        $subject_criteria = Subject::where('criteria_id', $id)
        // ->groupBy('id_teacher')
        // ->select('id_teacher')
        ->get();
        // dd($subject_criteria);
        $evalu_by_sub = Evaluation::where('status', 1)
        ->whereBetween('created_at', [ $date_start, $date_end ] )
        ->where('criteria_id', $id)
        // ->groupBy('id_teacher')
        // ->select('id_teacher')
        ->get();
        // dd($evalu_by_sub);
        foreach ($subject_criteria as $sub_criteria) {
            $evalu_by_sub = Evaluation::where('status', 1)
            ->where('id_subject', $sub_criteria->id)
            ->whereBetween('created_at', [ $date_start, $date_end ] )
            ->groupBy('id_teacher')
            ->select('id_teacher')
            ->get();
            // dd($evalu_by_sub);
            
        if (count($evalu_by_sub) == 0) {
            $by_criteria_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $by_criteria_devel[] = array('label' => "developing", 'y' => 0);
            $by_criteria_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $by_criteria_outst[] = array('label' => "Outstanding", 'y' => 0);
            $by_criteria_0[] = array('label' => $criteria, 'y' => 0);
            $new_total[] = array('label' => "Cap 1", 'y' => 0);
        } else {
            foreach ($evalu_by_sub as $evalu_by_teacher) {
                // dd($evalu_by_teacher);
                $med_for_teacher = Evaluation::where('status', 1)
                ->whereBetween('created_at', [ $date_start, $date_end ] )
                // ->where('grade', 2)
                // ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                ->get();
                // echo $med_for_teacher .'<br>';
                // dd(($med_for_teacher));
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            $total = $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
                                    + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    + $total_p5;
                            $tb = $total/count($med_for_teacher);
                            // dd($total);
                }
                $json_tb_l3 = json_encode($tb);
                $test_total[] = json_decode($tb);
                $med_total= array_sum($test_total);
                $chart_total_total[] = array('label' => $evalu_by_grade->teacher->fullname, 'y' => $tb);
            }
            $qqq = ($med_total/count($test_total));
            $in_total_total = 0;
            $de_total_total = 0;
            $ac_total_total = 0;
            $ou_total_total = 0;
            // dd($chart_total_total);
            foreach ($chart_total_total as $value) {
                // dd($value['y']);
                if ($value['y'] < $total_point*0.3) {
                    $in_total_total++;
                }
                if ($value['y'] >= $total_point*0.3 && $value['y'] < $total_point*0.6) {
                    $de_total_total++;
                }
                if ($value['y'] >= $total_point*0.6 && $value['y'] < $total_point*0.7) {
                    $ac_total_total++;
                }
                if ($value['y'] >= $total_point*0.7) {
                    $ou_total_total++;
                }
            }
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => $in_total_total);
            $chart_total_devel[] = array('label' => "developing", 'y' => $de_total_total);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => $ac_total_total);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => $ou_total_total);
            $chart_total_0[] = array('label' => "Total", 'y' => 0);
        }

        }



        $chart_teacher_ineff = json_encode($chart_teacher_ineff);
        $chart_teacher_accom = json_encode($chart_teacher_accom);
        $chart_teacher_outst = json_encode($chart_teacher_outst);
        $chart_teacher_devel = json_encode($chart_teacher_devel);
        $chart_teacher_0 = json_encode($chart_teacher_0);
        $chart_total_ineff = json_encode($chart_total_ineff);
        $chart_total_accom = json_encode($chart_total_accom);
        $chart_total_outst = json_encode($chart_total_outst);
        $chart_total_devel = json_encode($chart_total_devel);
        $chart_total_0 = json_encode($chart_total_0);
        // dd($chart_teacher_ineff);
        $new_evaluations = json_encode($new_evaluations);
                return view('be.charts.chart_by_grade',
                compact('new_evaluations', 'criteria',
                'chart_teacher_ineff', 'chart_teacher_devel', 'chart_teacher_accom', 'chart_teacher_outst', 'chart_teacher_0',
                'chart_total_ineff', 'chart_total_devel', 'chart_total_accom', 'chart_total_outst', 'chart_total_0'
            //     'location_l3', 'location_l4', 'location_l5',
            // 'evaluations_by_teacher_l3', 'evaluations_by_teacher_l4', 'evaluations_by_teacher_l5'
        ));
    }
    
    
    public function check()
    {
        
        $evaluations = Evaluation::where('status', 1)
                        ->where('id_location', 3)
                        ->where('id_teacher', 16)
                        ->get();
                        dd($evaluations);
        return view('be.charts.chart_all_location',
        compact('new_evaluations'));
    }
}
