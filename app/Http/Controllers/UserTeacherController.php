<?php

namespace App\Http\Controllers;

use App\User_teacher;
use App\Teacher;
use App\User;
use App\Course;
use Illuminate\Http\Request;

class UserTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('search.search');
    }

    public function search(Request $request)
    {
        if ($request->ajax()) {
            $output = '';
            $products = User::where('name', 'LIKE', '%' . $request->search . '%')->get();
            if ($products) {
                foreach ($products as $key => $product) {
                    $output .= '<tr>
                    <td>' . $product->id . '</td>
                    <td>' . $product->name . '</td>
                    <td>' . $product->email . '</td>
                    </tr>';
                }
            }
            
            return Response($output);
        }
    }
    public function post_search(Request $rq)
    {
        $products = User::where('name', 'LIKE', '%' . $rq->search . '%')->get();
        dd($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $user_teacher = new User_teacher();
        $user_teacher->teacher_id = $rq->teacher_id;
        $user_teacher->user_id = $rq->user_id;
        // dd($user_teacher);
        $isset_user_teacher = User_teacher::where('teacher_id', $rq->teacher_id)
        ->where('user_id', $rq->user_id)->get();
        // dd(count($isset_user_teacher));
        if (count($isset_user_teacher)>0) {
            return redirect()->back()->with('msg', 'Previewer have isset!');
        } else {
            $user_teacher->save();
            return redirect()->back()->with('msg', 'Add new success!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User_teacher  $user_teacher
     * @return \Illuminate\Http\Response
     */
    public function show(User_teacher $user_teacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User_teacher  $user_teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(User_teacher $user_teacher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User_teacher  $user_teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User_teacher $user_teacher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User_teacher  $user_teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($user, $teacher)
    {
        $user_teacher = User_teacher::where('user_id', $user)
                        ->where('teacher_id', $teacher)
                        ->first();
        // dd($user_teacher);
        $user_teacher->delete();
        return redirect()->back()->with('msg', 'Delete Success!');
    }
    
    public function import_json()
    {
        // echo getcwd();
        ini_set('max_execution_time', 3600);
        $json = file_get_contents('file.json');
        $json = json_decode($json, true);
        // dd($json);
        $i = 0;
        foreach ($json as $teacher_json) {
            // dd($student_json['id']);
            $teacher = Teacher::where('email', $teacher_json['teacher'])->first();
            $user = User::where('email', $teacher_json['admin'])->first();
            // dd($teacher);
            if ($teacher == null) {
                echo $i." : Không tìm thấy GV: ".($teacher_json['teacher'])."<br>";
                $i++;
                continue;
            }
            $user_teacher = new User_teacher();
            $user_teacher->teacher_id = $teacher->id;
            $user_teacher->user_id = $user->id;
            $user_teacher->save();
        }
    }
    
    public function update_course()
    {
        // echo getcwd();
        ini_set('max_execution_time', 3600);
        $json = file_get_contents('course_update.json');
        $json = json_decode($json, true);
        // dd($json);
        $i = 0;
        foreach ($json as $course_json) {
            // dd($student_json['id']);
            $course = Course::where('course_name', $course_json['code_class'])->first();
            // $user = User::where('email', $course_json['admin'])->first();
            // dd($course);
            if ($course == null) {
                echo $i." : Không tìm thấy GV: ".($course_json['teacher'])."<br>";
                $i++;
                continue;
            }
            $course->grade = $course_json['grade'];
            $course->update();
        }
    }

    public function teacher_user()
    {
        // echo getcwd();
        ini_set('max_execution_time', 3600);
        $json = file_get_contents('teacher_user.json');
        $json = json_decode($json, true);
        // dd($json);
        $i = 0;
        foreach ($json as $course_json) {
            // dd($student_json['id']);
            $user = User::where('email', $course_json['admin'])->first();
            // $user = User::where('email', $course_json['admin'])->first();
            // dd($user->id);
            if ($course_json['admin'] == '@theolympiaschools.edu.vn') {
                continue;
            }
            if ($user == null) {
                echo $i." : Không tìm thấy GV: ".($course_json['admin'])."<br>";
                $i++;
                continue;
            }
            $teacher_user = new User_teacher();
            $teacher_user->teacher_id = $course_json['teacher'];
            $teacher_user->user_id = $user->id;
            // $teacher_user->save();
        }
    }
}
