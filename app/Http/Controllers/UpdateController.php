<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;
use App\Session;
use App\Address;
use App\Teacher;
use App\Subject;
use App\Course;
use App\Evaluation;

class UpdateController extends Controller
{
    public function schedule()
    {
        // echo getcwd();
        ini_set('max_execution_time', 3600);
        
        $schedules = Schedule::all();
        foreach ($schedules as $schedule) {
            $course = Course::where('course_name', $schedule->class)->first();
            // dd($course);
            $i =0;
            if ($course == null) {
                echo $i." : Không tìm thấy GV: ".($schedule->id)."<br>";
                $i++;
                continue;
            }
            $schedule->class = $course->grade;
            $schedule->update();
        }
    }
    public function evaluation()
    {
        // echo getcwd();
        ini_set('max_execution_time', 3600);
        
        $evaluations = Evaluation::all();
        foreach ($evaluations as $evaluation) {
            $grade = $evaluation->schedule->class;
            // dd($grade);
            $i =0;
            if ($grade == null) {
                echo $i." : Không tìm thấy GV: ".($evaluation->id)."<br>";
                $i++;
                continue;
            }
            $evaluation->grade = $grade;
            $evaluation->update();
        }
    }
}
