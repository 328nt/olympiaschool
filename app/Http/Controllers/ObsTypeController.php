<?php

namespace App\Http\Controllers;

use App\obs_type;
use Illuminate\Http\Request;

class ObsTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\obs_type  $obs_type
     * @return \Illuminate\Http\Response
     */
    public function show(obs_type $obs_type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\obs_type  $obs_type
     * @return \Illuminate\Http\Response
     */
    public function edit(obs_type $obs_type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\obs_type  $obs_type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, obs_type $obs_type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\obs_type  $obs_type
     * @return \Illuminate\Http\Response
     */
    public function destroy(obs_type $obs_type)
    {
        //
    }
}
