<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evaluation;
use App\Schedule;
use App\Address;
use App\Criteria;
use App\Subject;
use App\Teacher;
use App\Obs_type;
use App\Card_options;
use App\User_teacher;
use Auth;
use Mail;

class ChartController extends Controller

{
    public function chart_criteria()
    {
        $criterias = Criteria::all();
        return view('be.charts.select_criteria', compact('criterias'));
    }
    
    
    public function chart_by_grade(Request $rq, $id)
    {
        // $id = $rq->criterias;
        $evaluations = Evaluation::all();
        $date_start = "2021-01-01 00:00:01";
        $date_end = "2021-01-31 23:59:01";
        
        $criteria = Criteria::find($id)->name;
        $components_1 = 54;
        if ($id == 1 || $id == 2) {
            $component_2_3 = 211;
        } elseif($id == 3) {
            $component_2_3 = 201;
        } elseif($id == 4) {
            $component_2_3 = 204;
        } elseif($id == 5) {
            $component_2_3 = 206;
        }

        $grades = Evaluation::where('status', 1)
        ->groupBy('grade')
        ->select('grade')
        ->get();
        // dd($grades);
        echo $grades .'<br>';
        if ($grades->isEmpty()) {
            return "fail";
        }
        
        $evaluation_total = Evaluation::where('status', 1)
        ->whereBetween('created_at', [ $date_start, $date_end ] )
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')
        ->get();
        // dd($evaluation_total);
        
        if (count($evaluation_total) == 0) {
            $by_criteria_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $by_criteria_devel[] = array('label' => "developing", 'y' => 0);
            $by_criteria_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $by_criteria_outst[] = array('label' => "Outstanding", 'y' => 0);
            $by_criteria_0[] = array('label' => $criteria, 'y' => 0);
            $new_total[] = array('label' => "Cap 1", 'y' => 0);
        } else {
            foreach ($evaluation_total as $evalu_by_teacher) {
                // dd($evalu_by_teacher);
                $med_for_teacher = Evaluation::where('status', 1)
                ->whereBetween('created_at', [ $date_start, $date_end ] )
                ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                ->get();
                // echo $med_for_teacher .'<br>';
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            $total = 
                                    // $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e;
                                    // + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    // + $total_p5;
                            $total_components_1 = $total_p1a + $total_p1b + $total_p1c;
                            $tb = $total/count($med_for_teacher);
                            $average_components_1 = $total_components_1/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_total[] = json_decode($tb);
                $med_total= array_sum($test_total);
                $chart_total_total[] = array('label' => $evalu_by_grade->teacher->email, 'y' => $tb);
                $chart_average_com_1[] = array('label' => $evalu_by_grade->teacher->email, 'y' => $average_components_1);
            }
            // dd($chart_average_com_1);
            $qqq = ($med_total/count($test_total));
            $in_total_total = 0;
            $de_total_total = 0;
            $ac_total_total = 0;
            $ou_total_total = 0;

            $in_com_1 = 0;
            $de_com_1 = 0;
            $ac_com_1 = 0;
            $ou_com_1 = 0;
            // dd($chart_total_total);
            foreach ($chart_total_total as $value) {
                // dd($value['y']);
                if ($value['y'] < $component_2_3*0.3) {
                    $in_total_total++;
                }
                if ($value['y'] >= $component_2_3*0.3 && $value['y'] < $component_2_3*0.6) {
                    $de_total_total++;
                }
                if ($value['y'] >= $component_2_3*0.6 && $value['y'] < $component_2_3*0.7) {
                    $ac_total_total++;
                }
                if ($value['y'] >= $component_2_3*0.7) {
                    $ou_total_total++;
                }
            }
            foreach ($chart_average_com_1 as $value) {
                // dd($value['y']);
                if ($value['y'] < $components_1*0.3) {
                    $in_com_1++;
                }
                if ($value['y'] >= $components_1*0.3 && $value['y'] < $components_1*0.6) {
                    $de_com_1++;
                }
                if ($value['y'] >= $components_1*0.6 && $value['y'] < $components_1*0.7) {
                    $ac_com_1++;
                }
                if ($value['y'] >= $components_1*0.7) {
                    $ou_com_1++;
                }
            }
            // dd($in_com_1);
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => $in_total_total);
            $chart_total_devel[] = array('label' => "developing", 'y' => $de_total_total);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => $ac_total_total);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => $ou_total_total);
            $chart_total_0[] = array('label' => "Totall", 'y' => 0);

            $chart_com1_ineff[] = array('label' => "Ineffective", 'y' => $in_com_1);
            $chart_com1_devel[] = array('label' => "developing", 'y' => $de_com_1);
            $chart_com1_accom[] = array('label' => "Accomplishing", 'y' => $ac_com_1);
            $chart_com1_outst[] = array('label' => "Outstanding", 'y' => $ou_com_1);
            $chart_total_com_1[] = array('label' => "Totall", 'y' => 0);
        }
        // dd($chart_total_0);
        //end total
        
        
        $evaluations_mn = Evaluation::where('status', 1)
        ->where('grade', 1)
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')->get();
        // dd(($evaluations_mn));
        if (count($evaluations_mn) == 0) {
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_total_devel[] = array('label' => "developing", 'y' => 0);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_total_0[] = array('label' => "Kindergarten", 'y' => 0);
            // $new_evaluations[] = array('label' => "Cap 1", 'y' => 0);
            $chart_com1_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_com1_devel[] = array('label' => "developing", 'y' => 0);
            $chart_com1_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_com1_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_total_com_1[] = array('label' => "Kindergarten", 'y' => 0);
        } else {
            foreach ($evaluations_mn as $evalu_by_teacher) {
                $med_for_teacher = Evaluation::where('status', 1)
                ->where('grade', 2)
                ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                // ->groupBy('part1', 'part2a')
                // ->select('part1', 'part2a')
                ->get();
                // echo $med_for_teacher .'<br>';
                // dd(($med_for_teacher));
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            // dd($total_p1);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            // dd($total_p5);
                            $total = $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e
                                    + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    + $total_p5;
                            $tb = $total/count($med_for_teacher);
                                    
                            $total_components_1 = $total_p1a + $total_p1b + $total_p1c;
                            $average_components_1 = $total_components_1/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_mn[] = json_decode($tb);
                $med_mn= array_sum($test_mn);
                $chart_total_mn[] = array('label' => $evalu_by_grade->teacher->fullname, 'y' => $tb);
                $chart_average_com_1_mn[] = array('label' => $evalu_by_grade->teacher->email, 'y' => $average_components_1);
            }
            $qqq = ($med_mn/count($test_mn));
            $in_mn = 0;
            $de_mn = 0;
            $ac_mn = 0;
            $ou_mn = 0;
            
            $in_com1_mn = 0;
            $de_com1_mn = 0;
            $ac_com1_mn = 0;
            $ou_com1_mn = 0;
            // dd($component_2_3);
            foreach ($chart_total_mn as $value) {
                // dd($value['y']);
                if ($value['y'] < $component_2_3*0.3) {
                    $in_mn++;
                }
                if ($value['y'] >= $component_2_3*0.3 && $value['y'] < $component_2_3*0.6) {
                    $de_mn++;
                }
                if ($value['y'] >= $component_2_3*0.6 && $value['y'] < $component_2_3*0.7) {
                    $ac_mn++;
                }
                if ($value['y'] >= $component_2_3*0.7) {
                    $ou_mn++;
                }
            }
            foreach ($chart_average_com_1_mn as $value) {
                // dd($value['y']);
                if ($value['y'] < $components_1*0.3) {
                    $in_com_1_mn++;
                }
                if ($value['y'] >= $components_1*0.3 && $value['y'] < $components_1*0.6) {
                    $de_com_1_mn++;
                }
                if ($value['y'] >= $components_1*0.6 && $value['y'] < $components_1*0.7) {
                    $ac_com_1_mn++;
                }
                if ($value['y'] >= $components_1*0.7) {
                    $ou_com_1_mn++;
                }
            }
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => $in_mn);
            $chart_total_devel[] = array('label' => "developing", 'y' => $de_mn);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => $ac_mn);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => $ou_mn);
            $chart_total_0[] = array('label' => "Kindergarten", 'y' => 0);
            
            $chart_com1_ineff[] = array('label' => "Ineffective", 'y' => $in_com_1_mn);
            $chart_com1_devel[] = array('label' => "developing", 'y' => $de_com_1_mn);
            $chart_com1_accom[] = array('label' => "Accomplishing", 'y' => $ac_com_1_mn);
            $chart_com1_outst[] = array('label' => "Outstanding", 'y' => $ou_com_1_mn);
            $chart_total_com_1[] = array('label' => "mn", 'y' => 0);
            // $new_evaluations[] = array('label' => "Cap 1", 'y' => $qqq);
            // dd($new_evaluations);
        }
        
        
        $teacher_c1 = Evaluation::where('status', 1)
        ->whereBetween('created_at', [ $date_start, $date_end ] )
        ->where('grade',2)
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')
        ->get();
        // dd($teacher_c1);
        if (count($teacher_c1) == 0) {
            $by_criteria_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $by_criteria_devel[] = array('label' => "developing", 'y' => 0);
            $by_criteria_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $by_criteria_outst[] = array('label' => "Outstanding", 'y' => 0);
            $by_criteria_0[] = array('label' => 'Primary', 'y' => 0);
            $new_total[] = array('label' => "Cap 1", 'y' => 0);
            $chart_com1_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_com1_devel[] = array('label' => "developing", 'y' => 0);
            $chart_com1_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_com1_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_total_com_1[] = array('label' => "Primary", 'y' => 0);
        } else {
            foreach ($teacher_c1 as $evalu_by_teacher) {
                // dd($evalu_by_teacher);
                $med_for_teacher = Evaluation::where('status', 1)
                ->whereBetween('created_at', [ $date_start, $date_end ] )
                ->where('grade',2)
                ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                ->get();
                // echo $med_for_teacher .'<br>';
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            $total = 
                                    // $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e;
                                    // + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    // + $total_p5;
                            $tb = $total/count($med_for_teacher);
                                    
                            $total_components_1 = $total_p1a + $total_p1b + $total_p1c;
                            $average_components_1 = $total_components_1/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_total[] = json_decode($tb);
                $med_total= array_sum($test_total);
                $chart_teacher_c1[] = array('label' => $evalu_by_grade->teacher->email, 'y' => $tb);
                $chart_average_com_1_c1[] = array('label' => $evalu_by_grade->teacher->email, 'y' => $average_components_1);
            }
            // dd($chart_teacher_c1);
            $qqq = ($med_total/count($test_total));
            $in_c1 = 0;
            $de_c1 = 0;
            $ac_c1 = 0;
            $ou_c1 = 0;
            // dd($chart_c1);
            foreach ($chart_teacher_c1 as $value) {
                // dd($value['y']);
                if ($value['y'] < $component_2_3*0.3) {
                    $in_c1++;
                }
                if ($value['y'] >= $component_2_3*0.3 && $value['y'] < $component_2_3*0.6) {
                    $de_c1++;
                }
                if ($value['y'] >= $component_2_3*0.6 && $value['y'] < $component_2_3*0.7) {
                    $ac_c1++;
                }
                if ($value['y'] >= $component_2_3*0.7) {
                    $ou_c1++;
                }
            }
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => $in_c1);
            $chart_total_devel[] = array('label' => "developing", 'y' => $de_c1);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => $ac_c1);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => $ou_c1);
            $chart_total_0[] = array('label' => "Primary", 'y' => 0);

            $in_com_1_c1 = 0;
            $de_com_1_c1 = 0;
            $ac_com_1_c1 = 0;
            $ou_com_1_c1 = 0;
            foreach ($chart_average_com_1_c1 as $value) {
                // dd($value['y']);
                if ($value['y'] < $components_1*0.3) {
                    $in_com_1_c1++;
                }
                if ($value['y'] >= $components_1*0.3 && $value['y'] < $components_1*0.6) {
                    $de_com_1_c1++;
                }
                if ($value['y'] >= $components_1*0.6 && $value['y'] < $components_1*0.7) {
                    $ac_com_1_c1++;
                }
                if ($value['y'] >= $components_1*0.7) {
                    $ou_com_1_c1++;
                }
            }
            $chart_com1_ineff[] = array('label' => "Ineffective", 'y' => $in_com_1_c1);
            $chart_com1_devel[] = array('label' => "developing", 'y' => $de_com_1_c1);
            $chart_com1_accom[] = array('label' => "Accomplishing", 'y' => $ac_com_1_c1);
            $chart_com1_outst[] = array('label' => "Outstanding", 'y' => $ou_com_1_c1);
            $chart_total_com_1[] = array('label' => "Primary", 'y' => 0);
        }
        
        
        $teacher_c2 = Evaluation::where('status', 1)
        ->whereBetween('created_at', [ $date_start, $date_end ] )
        ->where('grade',3)
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')
        ->get();
        // dd($teacher_c2);
        if (count($teacher_c2) == 0) {
            $by_criteria_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $by_criteria_devel[] = array('label' => "developing", 'y' => 0);
            $by_criteria_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $by_criteria_outst[] = array('label' => "Outstanding", 'y' => 0);
            $by_criteria_0[] = array('label' => 'Secondary', 'y' => 0);
            $new_total[] = array('label' => "Cap 1", 'y' => 0);

            $chart_com1_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_com1_devel[] = array('label' => "developing", 'y' => 0);
            $chart_com1_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_com1_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_total_com_1[] = array('label' => "Secondary", 'y' => 0);
        } else {
            foreach ($teacher_c2 as $evalu_by_teacher) {
                // dd($evalu_by_teacher);
                $med_for_teacher = Evaluation::where('status', 1)
                ->whereBetween('created_at', [ $date_start, $date_end ] )
                ->where('grade',3)
                ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                ->get();
                // echo $med_for_teacher .'<br>';
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            $total = 
                                    // $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e;
                                    // + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    // + $total_p5;
                            $tb = $total/count($med_for_teacher);
                            $total_components_1 = $total_p1a + $total_p1b + $total_p1c;
                            $average_components_1 = $total_components_1/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_total[] = json_decode($tb);
                $med_total= array_sum($test_total);
                $chart_teacher_c2[] = array('label' => $evalu_by_grade->teacher->email, 'y' => $tb);
                $chart_average_com_1_c2[] = array('label' => $evalu_by_grade->teacher->email, 'y' => $average_components_1);
            }
            // dd($chart_teacher_c2);
            $qqq = ($med_total/count($test_total));
            $in_c2 = 0;
            $de_c2 = 0;
            $ac_c2 = 0;
            $ou_c2 = 0;
            // dd($chart_c2);
            foreach ($chart_teacher_c2 as $value) {
                // dd($value['y']);
                if ($value['y'] < $component_2_3*0.3) {
                    $in_c2++;
                }
                if ($value['y'] >= $component_2_3*0.3 && $value['y'] < $component_2_3*0.6) {
                    $de_c2++;
                }
                if ($value['y'] >= $component_2_3*0.6 && $value['y'] < $component_2_3*0.7) {
                    $ac_c2++;
                }
                if ($value['y'] >= $component_2_3*0.7) {
                    $ou_c2++;
                }
            }
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => $in_c2);
            $chart_total_devel[] = array('label' => "developing", 'y' => $de_c2);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => $ac_c2);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => $ou_c2);
            $chart_total_0[] = array('label' => "Secondary", 'y' => 0);

            $in_com_1_c2 = 0;
            $de_com_1_c2 = 0;
            $ac_com_1_c2 = 0;
            $ou_com_1_c2 = 0;
            foreach ($chart_average_com_1_c2 as $value) {
                // dd($value['y']);
                if ($value['y'] < $components_1*0.3) {
                    $in_com_1_c2++;
                }
                if ($value['y'] >= $components_1*0.3 && $value['y'] < $components_1*0.6) {
                    $de_com_1_c2++;
                }
                if ($value['y'] >= $components_1*0.6 && $value['y'] < $components_1*0.7) {
                    $ac_com_1_c2++;
                }
                if ($value['y'] >= $components_1*0.7) {
                    $ou_com_1_c2++;
                }
            }
            $chart_com1_ineff[] = array('label' => "Ineffective", 'y' => $in_com_1_c2);
            $chart_com1_devel[] = array('label' => "developing", 'y' => $de_com_1_c2);
            $chart_com1_accom[] = array('label' => "Accomplishing", 'y' => $ac_com_1_c2);
            $chart_com1_outst[] = array('label' => "Outstanding", 'y' => $ou_com_1_c2);
            $chart_total_com_1[] = array('label' => "Secondary", 'y' => 0);
        }
        
        $teacher_c3 = Evaluation::where('status', 1)
        ->whereBetween('created_at', [ $date_start, $date_end ] )
        ->where('grade',4)
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')
        ->get();
        // dd($teacher_c3);
        if (count($teacher_c3) == 0) {
            $by_criteria_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $by_criteria_devel[] = array('label' => "developing", 'y' => 0);
            $by_criteria_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $by_criteria_outst[] = array('label' => "Outstanding", 'y' => 0);
            $by_criteria_0[] = array('label' => 'Highschool', 'y' => 0);
            $new_total[] = array('label' => "Cap 1", 'y' => 0);
            $chart_com1_ineff[] = array('label' => "Ineffective", 'y' => 0);
            $chart_com1_devel[] = array('label' => "developing", 'y' => 0);
            $chart_com1_accom[] = array('label' => "Accomplishing", 'y' => 0);
            $chart_com1_outst[] = array('label' => "Outstanding", 'y' => 0);
            $chart_total_com_1[] = array('label' => "Highschool", 'y' => 0);
        } else {
            foreach ($teacher_c3 as $evalu_by_teacher) {
                // dd($evalu_by_teacher);
                $med_for_teacher = Evaluation::where('status', 1)
                ->whereBetween('created_at', [ $date_start, $date_end ] )
                ->where('grade',4)
                ->where('criteria_id', $id)
                ->where('id_teacher', $evalu_by_teacher->id_teacher)
                ->get();
                // echo $med_for_teacher .'<br>';
                    $p1a1 = $p1a2 = $p1a3 = 0;
                    $p1b1 = $p1b2 = $p1c = 0;
                    $p2a1 = $p2a2 = $p2a3 = 0;
                    $p2b1 = $p2b2 = $p2c = 0;
                    $p2c1 = $p2c2 = $p2c3 = 0;
                    $p2d1 = $p2d2 = 0;
                    $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                    $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                    $p3d1 = $p3d2 = $p3e1 = 0;
                    $p4a = $p4b = $p4c = 0;
                    $p4d1 = $p4d2 = 0;
                    $p5a = $p5b = $p5c = 0;
                    $med = 0;
                    $basic_point = 0;
                foreach ($med_for_teacher as $evalu_by_grade) {
                            $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                            $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                            $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                            $total_p1a = ($p1a1+$p1a2 + $p1a3);
                            $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                            $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                            $total_p1b = ($p1b1+$p1b2);
                            $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                            $total_p1c = ($p1c);
                            $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                            $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                            $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                            $total_p2a = ($p2a1+$p2a2+$p2a3);
                            $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                            $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                            $total_p2b = ($p2b1+$p2b2);
                            $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                            $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                            $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                            $total_p2c = ($p2c1+$p2c2+$p2c3);
                            $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                            $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                            $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                            $total_p3a = ($p3a1+$p3a2+$p3a3);
                            $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                            $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                            $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                            $total_p3b = ($p3b1+$p3b2+$p3b3);
                            $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                            $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                            $total_p3c = ($p3c1+$p3c2);
                            $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                            $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                            $total_p3d = ($p3d1+$p3d2);
                            $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                            $total_p3e = ($p3e1);
                            $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                            $total_p4a = $p4a;
                            $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                            $total_p4b = $p4b;
                            $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                            $total_p4c = $p4c;
                            $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                            $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                            $total_p4d = ($p4d1+$p4d2);
                            $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                            $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                            $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                            $total_p5 = ($p5a+$p5b+$p5c);
                            $total = 
                                    // $total_p1a + $total_p1b + $total_p1c
                                    + $total_p2a + $total_p2b + $total_p2c 
                                    + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e;
                                    // + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                    // + $total_p5;
                            $tb = $total/count($med_for_teacher);
                            $total_components_1 = $total_p1a + $total_p1b + $total_p1c;
                            $average_components_1 = $total_components_1/count($med_for_teacher);
                }
                $json_tb_l3 = json_encode($tb);
                $test_total[] = json_decode($tb);
                $med_total= array_sum($test_total);
                $chart_teacher_c3[] = array('label' => $evalu_by_grade->teacher->email, 'y' => $tb);
                $chart_average_com_1_c3[] = array('label' => $evalu_by_grade->teacher->email, 'y' => $average_components_1);
            }
            // dd($chart_teacher_c3);
            $qqq = ($med_total/count($test_total));
            $in_c3 = 0;
            $de_c3 = 0;
            $ac_c3 = 0;
            $ou_c3 = 0;
            // dd($chart_c3);
            foreach ($chart_teacher_c3 as $value) {
                // dd($value['y']);
                if ($value['y'] < $component_2_3*0.3) {
                    $in_c3++;
                }
                if ($value['y'] >= $component_2_3*0.3 && $value['y'] < $component_2_3*0.6) {
                    $de_c3++;
                }
                if ($value['y'] >= $component_2_3*0.6 && $value['y'] < $component_2_3*0.7) {
                    $ac_c3++;
                }
                if ($value['y'] >= $component_2_3*0.7) {
                    $ou_c3++;
                }
            }
            $chart_total_ineff[] = array('label' => "Ineffective", 'y' => $in_c3);
            $chart_total_devel[] = array('label' => "developing", 'y' => $de_c3);
            $chart_total_accom[] = array('label' => "Accomplishing", 'y' => $ac_c3);
            $chart_total_outst[] = array('label' => "Outstanding", 'y' => $ou_c3);
            $chart_total_0[] = array('label' => "Highschool", 'y' => 0);
            
            $in_com_1_c3 = 0;
            $de_com_1_c3 = 0;
            $ac_com_1_c3 = 0;
            $ou_com_1_c3 = 0;
            foreach ($chart_average_com_1_c3 as $value) {
                // dd($value['y']);
                if ($value['y'] < $components_1*0.3) {
                    $in_com_1_c3++;
                }
                if ($value['y'] >= $components_1*0.3 && $value['y'] < $components_1*0.6) {
                    $de_com_1_c3++;
                }
                if ($value['y'] >= $components_1*0.6 && $value['y'] < $components_1*0.7) {
                    $ac_com_1_c3++;
                }
                if ($value['y'] >= $components_1*0.7) {
                    $ou_com_1_c3++;
                }
            }
            $chart_com1_ineff[] = array('label' => "Ineffective", 'y' => $in_com_1_c3);
            $chart_com1_devel[] = array('label' => "developing", 'y' => $de_com_1_c3);
            $chart_com1_accom[] = array('label' => "Accomplishing", 'y' => $ac_com_1_c3);
            $chart_com1_outst[] = array('label' => "Outstanding", 'y' => $ou_com_1_c3);
            $chart_total_com_1[] = array('label' => "Highschool", 'y' => 0);
        }

        $chart_total_ineff = json_encode($chart_total_ineff);
        $chart_total_accom = json_encode($chart_total_accom);
        $chart_total_outst = json_encode($chart_total_outst);
        $chart_total_devel = json_encode($chart_total_devel);
        $chart_total_0 = json_encode($chart_total_0);
        
        $chart_com1_ineff = json_encode($chart_com1_ineff);
        $chart_com1_accom = json_encode($chart_com1_accom);
        $chart_com1_outst = json_encode($chart_com1_outst);
        $chart_com1_devel = json_encode($chart_com1_devel);

        $chart_total_com_1 = json_encode($chart_total_com_1);
        // dd(json_decode($chart_total_com_1));
        // $new_evaluations = json_encode($new_evaluations);
                return view('be.charts.chart_by_grade',
                compact( 'criteria',
                'chart_total_ineff', 'chart_total_devel', 'chart_total_accom', 'chart_total_outst', 'chart_total_0',
                'chart_com1_ineff', 'chart_com1_devel', 'chart_com1_accom', 'chart_com1_outst', 'chart_total_com_1',
            //     'location_l3', 'location_l4', 'location_l5',
            // 'evaluations_by_teacher_l3', 'evaluations_by_teacher_l4', 'evaluations_by_teacher_l5'
        ));
    }
    
    public function chart_by_subject(Request $rq, $id)
    {
        $evaluations = Evaluation::all();
        $date_start = "2021-01-01 00:00:01";
        $date_end = "2021-01-31 23:59:01";
        
        $criteria = Criteria::find($id)->name;
        $components_1 = 54;
        if ($id == 1 || $id == 2) {
            $component_2_3 = 211;
        } elseif($id == 3) {
            $component_2_3 = 201;
        } elseif($id == 4) {
            $component_2_3 = 204;
        } elseif($id == 5) {
            $component_2_3 = 206;
        }
        $subjects = Subject::where('criteria_id', $id)->get();
        // dd($subjects);
        foreach ($subjects as $subject) {
            $teachers[] = Evaluation::where('status', 1)
            ->whereBetween('created_at', [ $date_start, $date_end ] )
            ->where('id_subject', $subject->id)
            ->groupBy('id_teacher')
            ->select('id_teacher')
            ->get();
            // dd($teachers);
            if (count($teachers) == 0) {
                break;
                $by_criteria_ineff[] = array('label' => "Ineffective", 'y' => 0);
                $by_criteria_devel[] = array('label' => "developing", 'y' => 0);
                $by_criteria_accom[] = array('label' => "Accomplishing", 'y' => 0);
                $by_criteria_outst[] = array('label' => "Outstanding", 'y' => 0);
                $by_criteria_0[] = array('label' => $subject->name, 'y' => 0);
            } else {
                foreach ($teachers as $evalu_by_teacher) {
                    // dd($evalu_by_teacher);
                    $med_for_teacher = Evaluation::where('status', 1)
                    ->whereBetween('created_at', [ $date_start, $date_end ] )
                    ->where('id_subject', $subject->id)
                    ->where('id_teacher', $evalu_by_teacher->id_teacher)
                    ->get();
                    // echo $med_for_teacher .'<br>';
                        $p1a1 = $p1a2 = $p1a3 = 0;
                        $p1b1 = $p1b2 = $p1c = 0;
                        $p2a1 = $p2a2 = $p2a3 = 0;
                        $p2b1 = $p2b2 = $p2c = 0;
                        $p2c1 = $p2c2 = $p2c3 = 0;
                        $p2d1 = $p2d2 = 0;
                        $p3a1 = $p3a2 = $p3a3 = $p3a4 = 0;
                        $p3b1 = $p3b2 = $p3b3 = $p3c1 = $p3c2 = 0;
                        $p3d1 = $p3d2 = $p3e1 = 0;
                        $p4a = $p4b = $p4c = 0;
                        $p4d1 = $p4d2 = 0;
                        $p5a = $p5b = $p5c = 0;
                        $med = 0;
                        $basic_point = 0;
                    foreach ($med_for_teacher as $evalu_by_grade) {
                                $p1a1+= array_sum($evalu_by_grade->part1['p1a1']);
                                $p1a2+= array_sum($evalu_by_grade->part1['p1a2']);
                                $p1a3+= array_sum($evalu_by_grade->part1['p1a3']);
                                $total_p1a = ($p1a1+$p1a2 + $p1a3);
                                $p1b1+= array_sum($evalu_by_grade->part1['p1b1']);
                                $p1b2+= array_sum($evalu_by_grade->part1['p1b2']);
                                $total_p1b = ($p1b1+$p1b2);
                                $p1c+= array_sum($evalu_by_grade->part1['p1c']);
                                $total_p1c = ($p1c);
                                $p2a1+= array_sum($evalu_by_grade->part2['p2a1']);
                                $p2a2+= array_sum($evalu_by_grade->part2['p2a2']);
                                $p2a3+= array_sum($evalu_by_grade->part2['p2a3']);
                                $total_p2a = ($p2a1+$p2a2+$p2a3);
                                $p2b1+= array_sum($evalu_by_grade->part2['p2b1']);
                                $p2b2+= array_sum($evalu_by_grade->part2['p2b2']);
                                $total_p2b = ($p2b1+$p2b2);
                                $p2c1+= array_sum($evalu_by_grade->part2['p2c1']);
                                $p2c2+= array_sum($evalu_by_grade->part2['p2c2']);
                                $p2c3+= array_sum($evalu_by_grade->part2['p2c3']);
                                $total_p2c = ($p2c1+$p2c2+$p2c3);
                                $p3a1+= array_sum($evalu_by_grade->part3['p3a1']);
                                $p3a2+= array_sum($evalu_by_grade->part3['p3a2']);
                                $p3a3+= array_sum($evalu_by_grade->part3['p3a3']);
                                $total_p3a = ($p3a1+$p3a2+$p3a3);
                                $p3b1+= array_sum($evalu_by_grade->part3['p3b1']);
                                $p3b2+= array_sum($evalu_by_grade->part3['p3b2']);
                                $p3b3+= array_sum($evalu_by_grade->part3['p3b3']);
                                $total_p3b = ($p3b1+$p3b2+$p3b3);
                                $p3c1+= array_sum($evalu_by_grade->part3['p3c1']);
                                $p3c2+= array_sum($evalu_by_grade->part3['p3c2']);
                                $total_p3c = ($p3c1+$p3c2);
                                $p3d1+= array_sum($evalu_by_grade->part3['p3d1']);
                                $p3d2+= array_sum($evalu_by_grade->part3['p3d2']);
                                $total_p3d = ($p3d1+$p3d2);
                                $p3e1+= array_sum($evalu_by_grade->part3['p3e1']);
                                $total_p3e = ($p3e1);
                                $p4a+= array_sum($evalu_by_grade->part4['p4a']);
                                $total_p4a = $p4a;
                                $p4b+= array_sum($evalu_by_grade->part4['p4b']);
                                $total_p4b = $p4b;
                                $p4c+= array_sum($evalu_by_grade->part4['p4c']);
                                $total_p4c = $p4c;
                                $p4d1+= array_sum($evalu_by_grade->part4['p4d1']);
                                $p4d2+= array_sum($evalu_by_grade->part4['p4d2']);
                                $total_p4d = ($p4d1+$p4d2);
                                $p5a+= array_sum($evalu_by_grade->part5['p5a']);
                                $p5b+= array_sum($evalu_by_grade->part5['p5b']);
                                $p5c+= array_sum($evalu_by_grade->part5['p5c']);
                                $total_p5 = ($p5a+$p5b+$p5c);
                                $total = 
                                        // $total_p1a + $total_p1b + $total_p1c
                                        + $total_p2a + $total_p2b + $total_p2c 
                                        + $total_p3a + $total_p3b + $total_p3c + $total_p3d + $total_p3e;
                                        // + $total_p4a + $total_p4b + $total_p4c + $total_p4d
                                        // + $total_p5;
                                $total_components_1 = $total_p1a + $total_p1b + $total_p1c;
                                $tb = $total/count($med_for_teacher);
                                $average_components_1 = $total_components_1/count($med_for_teacher);
                    }
                    $chart_subject_total[] = array('label' => $evalu_by_grade->teacher->email, 'y' => $tb);
                    $chart_average_com_1[] = array('label' => $evalu_by_grade->teacher->email, 'y' => $average_components_1);
                }
                $in_subject = 0;
                $de_subject = 0;
                $ac_subject = 0;
                $ou_subject = 0;
                // dd($chart_subject_total);
                foreach ($chart_subject_total as $value) {
                    // dd($value['y']);
                    if ($value['y'] < $component_2_3*0.3) {
                        $in_subject++;
                    }
                    if ($value['y'] >= $component_2_3*0.3 && $value['y'] < $component_2_3*0.6) {
                        $de_subject++;
                    }
                    if ($value['y'] >= $component_2_3*0.6 && $value['y'] < $component_2_3*0.7) {
                        $ac_subject++;
                    }
                    if ($value['y'] >= $component_2_3*0.7) {
                        $ou_subject++;
                    }
                }
                $chart_subject_ineff[] = array('label' => "Ineffective", 'y' => $in_subject);
                $chart_subject_devel[] = array('label' => "developing", 'y' => $de_subject);
                $chart_subject_accom[] = array('label' => "Accomplishing", 'y' => $ac_subject);
                $chart_subject_outst[] = array('label' => "Outstanding", 'y' => $ou_subject);
                $chart_subject[] = array('label' => $subject->name, 'y' => 0);
    
            }
            dd($chart_subject_total);
        }
        
        dd($teachers);
        $chart_subject_ineff = json_encode($chart_subject_ineff);
        $chart_subject_accom = json_encode($chart_subject_accom);
        $chart_subject_outst = json_encode($chart_subject_outst);
        $chart_subject_devel = json_encode($chart_subject_devel);
        $chart_subject_0 = json_encode($chart_subject);
        
        
        $evalu_by_sub = Evaluation::where('status', 1)
        ->whereBetween('created_at', [ $date_start, $date_end ] )
        ->where('criteria_id', $id)
        ->groupBy('id_teacher')
        ->select('id_teacher')
        ->get();
        // dd($evalu_by_sub);
        return view('be.charts.chart_by_subject',
                compact( 'criteria',
                'chart_subject_ineff', 'chart_subject_devel', 'chart_subject_accom', 'chart_subject_outst', 'chart_subject',
                // 'chart_com1_ineff', 'chart_com1_devel', 'chart_com1_accom', 'chart_com1_outst', 'chart_total_com_1',
            //     'location_l3', 'location_l4', 'location_l5',
            // 'evaluations_by_teacher_l3', 'evaluations_by_teacher_l4', 'evaluations_by_teacher_l5'
        ));

    }
    
    
    public function check()
    {
        
        $evaluations = Evaluation::where('status', 1)
                        ->where('id_location', 3)
                        ->where('id_teacher', 16)
                        ->get();
                        dd($evaluations);
        return view('be.charts.chart_all_location',
        compact('new_evaluations'));
    }
}
