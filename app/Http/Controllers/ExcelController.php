<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\TeachersImport;
use App\Imports\SessionImport;
use App\Imports\UserImport;
use App\Imports\SubjectsImport;
use App\Imports\DefaultScheduleImport;
use App\Exports\COTExport;
use App\Exports\ScheduleExport;
use App\Exports\List_view;
use App\Evaluation;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function importExportView()
    {
        return view('excel.excel');
    }
    
    public function export_cot()
    {
        return view('excel.export_cot');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export(Request $rq) 
    {
        // dd($rq->all() ) ;
        ini_set('max_execution_time', 2000);
        set_time_limit(2000);
        return Excel::download(new COTExport, 'TOC.xlsx');
    }
    public function schedule_export() 
    {
        // dd($rq->all() ) ;
        ini_set('max_execution_time', 2000);
        set_time_limit(2000);
        return Excel::download(new ScheduleExport, 'schedule.xlsx');
    }
    public function list_view() 
    {
        // dd($rq->all() ) ;
        return Excel::download(new List_view, 'teacher_email.xlsx');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function schedulesimport() 
    {
        Excel::import(new DefaultScheduleImport,request()->file('file'));
        
        return back()->with('msg', 'Import Success!');
    }
    
    public function sessionimport() 
    {
        Excel::import(new SessionImport,request()->file('file'));
        
        return back()->with('msg', 'Import Success!');
    }
    
    public function userimport() 
    {
        try {
            Excel::import(new UserImport,request()->file('file'));
            return back()->with('msg', 'Import Success!');
        } catch(\Illuminate\Database\QueryException $ex){
            return back()->with('msg', $ex->getMessage());
        }
        
    }
    
    public function subjectimport() 
    {
        // dd(request()->file('file'));
        Excel::import(new SubjectsImport,request()->file('file'));
        
        return back()->with('msg', 'Import Success!');
    }
    public function teacherimport() 
    {
        // dd(request()->file('file'));
        Excel::import(new TeachersImport,request()->file('file'));
        
        return back()->with('msg', 'Import Success!');
    }

    public function teacher_user()
    {
        // echo getcwd();
        ini_set('max_execution_time', 3600);
        $json = file_get_contents('user_update.json');
        $json = json_decode($json, true);
        // dd($json);
        $i = 0;
        foreach ($json as $course_json) {
            // dd($student_json['id']);
            // $course = Course::where('course_name', $course_json['code_class'])->first();
            $user = User::where('email', $course_json['email'])->first();
            // dd($course);
            if ($user == null) {
                echo $i." : Không tìm thấy GV: ".($course_json['email'])."<br>";
                $i++;
                continue;
            }
            $user->role = 1;
            $user->update();
        }
    }
}
