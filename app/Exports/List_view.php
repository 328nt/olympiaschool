<?php

namespace App\Exports;

use App\Evaluation;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class List_view implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     return Evaluation::all();
    // }
    public function view(): View
    {
        $evaluations = DB::table('table_name')->get();
        // dd($evaluations);
        return view('excel.list_view', [
            'list' => DB::table('table_name')
            ->get()
            
        ]);
    }
}
