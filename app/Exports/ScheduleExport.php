<?php

namespace App\Exports;

use App\Schedule;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ScheduleExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('excel.export_schedule', [
            // 'evaluations' => Evaluation::all();
            'schedules' => Schedule::all()
            
        ]);
    }
}
