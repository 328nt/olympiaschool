<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_teacher extends Model
{
    
    public $table = "user_teachers";
    protected $fillable = [
        'teacher_id', 'user_id'
    ];
}
