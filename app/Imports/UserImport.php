<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UserImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'role'  => $row['role'],
            'name' => $row['name'],
            'email' => $row['email'],
            'image' => 'default.png',
            'active' => $row['active'],
            'qa-qc' => $row['qaqc'],
            'password'    => bcrypt(123456),
        ]);
    }
}
