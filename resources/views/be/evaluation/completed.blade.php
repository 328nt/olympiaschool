@extends('be/layouts/index')
@section('title')
Evaluation Completed
@endsection
@section('style')

<style>
    #customers {
        border-collapse: collapse;
        width: 100%;
        text-align: center;
        /* width: 50%; */
    }

    #customers td,
    #customers th {
        border: 1px solid #00486e;
        padding: 8px;
    }


    .solid {
        border: 1px solid #00486e;
        padding: 10px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: #00486e;
        color: white;
    }

    .b_ddd {
        border: 1px solid #ddd;
        padding: 5px;
    }

    .p_top10 {
        padding-top: 10px;
    }

    .p_top5 {
        padding-top: 5px;
    }

    .pd10 {
        padding: 30px;
    }

    .table-striped>tbody>tr:nth-child(odd)>td,
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #d3e1ff;
    }

    .table-striped>tbody>tr:nth-child(even)>td,
    .table-striped>tbody>tr:nth-child(even)>th {
        background-color: #fff;
    }

    .table-bordered>tbody>tr>td {
        border: 1px solid #0000 !important;
    }
</style>
@endsection
@section('content')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <form role="search" class="sr-input-func">
                                    <input type="text" placeholder="Search..." class="search-int form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                </li>
                                <li><span class="bread-blod">Accordion</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- accordion start-->
<div class="edu-accordion-area mg-b-15">
    <div class="container-fluid">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row pd10">
                <div class="admin-pro-accordion-wrap shadow-inner responsive-mg-b-30">
                    <div class="panel-group edu-custon-design" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading accordion-head" style="text-align:center;">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                        COMPLETED</a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse panel-ic collapse in">
                                <div class="panel-body admin-panel-content animated bounce">
                                    <div class="row" style="overflow-x:auto;">
                                        <table id="customers" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Card</th>
                                                    <th>Subject</th>
                                                    <th>Date</th>
                                                    <th>Teacher</th>
                                                    <th>Booking</th>
                                                    <th>Class</th>
                                                    <th>Total</th>
                                                    <th>Completed At</th>
                                                    <th>Action</th>
                                                    @if (Auth::user()->role == 1)
                                                    <th>Delete</th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($evaluations as $evaluation)
                                                {{-- @if ($evaluation->grade == 2) --}}

                                                <tr>
                                                    <td>{{$evaluation->id}}</td>
                                                    <td>
                                                        @if ($evaluation->card_option_id != null)
                                                        @if ($evaluation->card_option->card_id == 1)
                                                        <img src="images/flag-red.png" style="width: 20px" alt="">
                                                        @elseif($evaluation->card_option->card_id == 2)
                                                        <img src="images/flag-yellow.png" style="width: 20px" alt="">
                                                        @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{$evaluation->schedule->subject->name}}
                                                        <br>
                                                        {{$evaluation->criteria->name}}
                                                    </td>
                                                    <td>{{date('d/m/Y',strtotime($evaluation->schedule->time1))}}</td>
                                                    <td>
                                                        {{$evaluation->teacher->fullname}}
                                                        <br>
                                                        {{strstr($evaluation->teacher->email,"@", true)}}
                                                    </td>
                                                    <td>
                                                        {{$evaluation->user->name}}
                                                        <br>
                                                        {{strstr($evaluation->user->email,"@", true)}}
                                                    </td>
                                                    <td>{{$evaluation->schedule->class}}</td>
                                                    <td>
                                                        {{$total =  
                                                        array_sum($evaluation->part1['p1a1']) + array_sum($evaluation->part1['p1a2']) + array_sum($evaluation->part1['p1a3']) +
                                                        array_sum($evaluation->part1['p1b1']) + array_sum($evaluation->part1['p1b2']) + array_sum($evaluation->part1['p1c']) +
                                                        array_sum($evaluation->part2['p2a1']) + array_sum($evaluation->part2['p2a2']) + array_sum($evaluation->part2['p2a3']) +
                                                        array_sum($evaluation->part2['p2b1']) + array_sum($evaluation->part2['p2b2']) + 
                                                        array_sum($evaluation->part2['p2c1']) + array_sum($evaluation->part2['p2c2']) + array_sum($evaluation->part2['p2c3']) +
                                                        
                                                        array_sum($evaluation->part3['p3a1']) + array_sum($evaluation->part3['p3a2']) + array_sum($evaluation->part3['p3a3']) +
                                                        array_sum($evaluation->part3['p3b1']) + array_sum($evaluation->part3['p3b2']) + array_sum($evaluation->part3['p3b3']) +
                                                        array_sum($evaluation->part3['p3c1']) + array_sum($evaluation->part3['p3c2']) + 
                                                        array_sum($evaluation->part3['p3d1']) + array_sum($evaluation->part3['p3d2']) + 
                                                        array_sum($evaluation->part3['p3e1']) +
                                                        array_sum($evaluation->part4['p4a']) + array_sum($evaluation->part4['p4b']) + array_sum($evaluation->part4['p4c']) + 
                                                        array_sum($evaluation->part4['p4d1']) + array_sum($evaluation->part4['p4d2']) + 
                                                        array_sum($evaluation->part5['p5a']) + array_sum($evaluation->part5['p5b']) + array_sum($evaluation->part5['p5c'])
                                                                    }}
                                                    </td>
                                                    <td>
                                                        {{date('H:i d/m/Y',strtotime($evaluation->updated_at))}}
                                                    </td>
                                                    <td>
                                                        <a href="{{route('evaluation_view',$evaluation->id)}}"
                                                            data-toggle="tooltip" title="Draft" class="pd-setting-ed"><i
                                                                class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Info</a>
                                                    </td>
                                                    @if (Auth::user()->role == 1)
                                                    <td>
                                                        <a href="{{route('evaluation_destroy', $evaluation->id)}}"
                                                            onclick="return confirm('Are you sure you want to delete this evaluation?')"
                                                            data-toggle="tooltip" title="Delete"
                                                            class="pd-setting-ed"><i class="fa fa-trash-o"
                                                                aria-hidden="true"></i>
                                                            Delete</a>
                                                    </td>
                                                    @endif
                                                </tr>
                                                {{-- @endif --}}
                                                @empty
                                                <tr>
                                                    <td>No data found !</td>
                                                </tr>
                                                @endforelse
                                                @if (count(Auth::user()->teacher)>0)
                                                @foreach (Auth::user()->teacher as $teacher)
                                                @php
                                                $list_evalu = App\Evaluation::where('status', 1)
                                                ->where('id_teacher', $teacher->id)
                                                ->get();
                                                // dd($list_evalu);
                                                @endphp
                                                @foreach ($list_evalu as $item)
                                                <tr>
                                                    <td>{{$item->id}}</td>
                                                    <td>
                                                        @if ($item->card_option_id != null)
                                                        @if ($item->card_option->card_id == 1)
                                                        <img src="images/flag-red.png" style="width: 20px" alt="">
                                                        @elseif($item->card_option->card_id == 2)
                                                        <img src="images/flag-yellow.png" style="width: 20px" alt="">
                                                        @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{$item->schedule->subject->name}}/{{$item->criteria->name}}
                                                    </td>
                                                    <td>{{date('d/m/Y',strtotime($item->schedule->time1))}}</td>
                                                    <td>{{$item->teacher->fullname}}
                                                        <br>
                                                        {{strstr($item->teacher->email,"@", true)}}</td>
                                                    <td>
                                                        {{$item->user->name}}
                                                        <br>
                                                        {{strstr($item->user->email,"@", true)}}
                                                    </td>
                                                    <td>{{$item->schedule->class}}</td>
                                                    <td>
                                                        {{$total =  
                                                            array_sum($item->part1['p1a1']) + array_sum($item->part1['p1a2']) + array_sum($item->part1['p1a3']) +
                                                            array_sum($item->part1['p1b1']) + array_sum($item->part1['p1b2']) + array_sum($item->part1['p1c']) +
                                                            array_sum($item->part2['p2a1']) + array_sum($item->part2['p2a2']) + array_sum($item->part2['p2a3']) +
                                                            array_sum($item->part2['p2b1']) + array_sum($item->part2['p2b2']) + 
                                                            array_sum($item->part2['p2c1']) + array_sum($item->part2['p2c2']) + array_sum($item->part2['p2c3']) +
                                                            
                                                            array_sum($item->part3['p3a1']) + array_sum($item->part3['p3a2']) + array_sum($item->part3['p3a3']) +
                                                            array_sum($item->part3['p3b1']) + array_sum($item->part3['p3b2']) + array_sum($item->part3['p3b3']) +
                                                            array_sum($item->part3['p3c1']) + array_sum($item->part3['p3c2']) + 
                                                            array_sum($item->part3['p3d1']) + array_sum($item->part3['p3d2']) + 
                                                            array_sum($item->part3['p3e1']) +
                                                            array_sum($item->part4['p4a']) + array_sum($item->part4['p4b']) + array_sum($item->part4['p4c']) + 
                                                            array_sum($item->part4['p4d1']) + array_sum($item->part4['p4d2']) + 
                                                            array_sum($item->part5['p5a']) + array_sum($item->part5['p5b']) + array_sum($item->part5['p5c'])
                                                                        }}
                                                    </td>
                                                    <td>
                                                        {{date('H:i d/m/Y',strtotime($evaluation->updated_at))}}
                                                    </td>
                                                    <td>
                                                        <a href="{{route('evaluation_view',$item->id)}}"
                                                            data-toggle="tooltip" title="Draft" class="pd-setting-ed"><i
                                                                class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Info</a>
                                                    </td>
                                                    @if (Auth::user()->role == 1)
                                                    <td>
                                                        <a href="{{route('evaluation_destroy', $item->id)}}"
                                                            onclick="return confirm('Are you sure you want to delete this evaluation?')"
                                                            data-toggle="tooltip" title="Delete"
                                                            class="pd-setting-ed"><i class="fa fa-trash-o"
                                                                aria-hidden="true"></i>
                                                            Delete</a>
                                                    </td>
                                                    @endif
                                                </tr>
                                                @endforeach
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                        <script>
                                            $(document).ready(function() {
                                                    $('#customers').DataTable();
                                                } );
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection