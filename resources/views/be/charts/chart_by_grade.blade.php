@extends('be/layouts/index')
@section('title')
Chart all location
@endsection
@section('style')

<style>
    #customers {
        border-collapse: collapse;
        width: 100%;
        text-align: center;
        /* width: 50%; */
    }

    #customers td,
    #customers th {
        border: 1px solid #a1a1a1;
        padding: 5px;
    }


    .solid {
        border: 1px solid #00486e;
        padding: 10px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: center;
        background-color: #00486e;
        color: white;
    }

    .b_ddd {
        border: 1px solid #ddd;
        padding: 5px;
    }

    .p_top10 {
        padding-top: 10px;
    }

    .p_top5 {
        padding-top: 5px;
    }

    .pd10 {
        padding: 30px;
    }
</style>
@endsection
@section('content')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <form role="search" class="sr-input-func">
                                    <input type="text" placeholder="Search..." class="search-int form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <ul class="breadcome-menu">
                                <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                </li>
                                <li><span class="bread-blod">Accordion</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- accordion start-->
            <script type="text/javascript">
                window.onload = function () {
        
                        var criteria_per_total = new CanvasJS.Chart("criteria_per_total",
                        {
                            title:{
                                text: "{{$criteria}}/Components 2+3"
                            },
                            axisX:{
                                title: "Cấp độ"
                            },
                            axisY:{
                                title: "percentage"
                            },
                            data: [
                            {
                                type: "stackedColumn100",
                                legendText: "Ineffective",
                                showInLegend: "true",
                                indexLabel: "{y}",
                                indexLabelPlacement: "inside",
                                indexLabelFontColor: "white",
                                dataPoints:  {!!$chart_total_ineff!!}
                            },
                            {
                                type: "stackedColumn100",
                                legendText: "Developing",
                                showInLegend: "true",
                                indexLabel: "{y}",
                                indexLabelPlacement: "inside",
                                indexLabelFontColor: "white",
                                dataPoints: {!!$chart_total_devel!!}
                            },
                            {
                                type: "stackedColumn100",
                                legendText: "Accomplishing",
                                showInLegend: "true",
                                indexLabel: "{y}",
                                indexLabelPlacement: "inside",
                                indexLabelFontColor: "white",
                                dataPoints: {!!$chart_total_accom!!}
                            },
                            {
                                type: "stackedColumn100",
                                legendText: "Outstanding",
                                showInLegend: "true",
                                indexLabel: "{y}",
                                indexLabelPlacement: "inside",
                                indexLabelFontColor: "white",
                                dataPoints: {!!$chart_total_outst!!}
                            },
                            {
                                type: "stackedColumn100",
                                // legendText: "Chart",
                                // showInLegend: "false",
                                indexLabel: "{y}",
                                indexLabelPlacement: "inside",
                                indexLabelFontColor: "white",
                                dataPoints: {!!$chart_total_0!!}
                            }
                            ]
                        });
                        
                        var criteria_per_com_1 = new CanvasJS.Chart("criteria_per_com_1",
                        {
                            title:{
                                text: "{{$criteria}}/Components 1"
                            },
                            axisX:{
                                title: "Cấp độ"
                            },
                            axisY:{
                                title: "percentage"
                            },
                            data: [
                            {
                                type: "stackedColumn100",
                                legendText: "Ineffective",
                                showInLegend: "true",
                                indexLabel: "{y}",
                                indexLabelPlacement: "inside",
                                indexLabelFontColor: "white",
                                dataPoints:  {!!$chart_com1_ineff!!}
                            },
                            {
                                type: "stackedColumn100",
                                legendText: "Developing",
                                showInLegend: "true",
                                indexLabel: "{y}",
                                indexLabelPlacement: "inside",
                                indexLabelFontColor: "white",
                                dataPoints: {!!$chart_com1_devel!!}
                            },
                            {
                                type: "stackedColumn100",
                                legendText: "Accomplishing",
                                showInLegend: "true",
                                indexLabel: "{y}",
                                indexLabelPlacement: "inside",
                                indexLabelFontColor: "white",
                                dataPoints: {!!$chart_com1_accom!!}
                            },
                            {
                                type: "stackedColumn100",
                                legendText: "Outstanding",
                                showInLegend: "true",
                                indexLabel: "{y}",
                                indexLabelPlacement: "inside",
                                indexLabelFontColor: "white",
                                dataPoints: {!!$chart_com1_outst!!}
                            },
                            {
                                type: "stackedColumn100",
                                indexLabel: "{y}",
                                indexLabelPlacement: "inside",
                                indexLabelFontColor: "white",
                                dataPoints: {!!$chart_total_com_1!!}
                            }
                            ]
                        });
                        // chart_total.render();
                        // chart.render();
                        criteria_per_com_1.render();
                        criteria_per_total.render();
                        
                    }
            </script>
<div class="edu-accordion-area mg-b-15">
    <div class="container">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="panel-heading accordion-head" style="text-align:center;">
                <h3>
                    <b>
                        Classification by Levels (Components 1)
                    </b>
                </h3>
            </div>
            <table id="customers" class="table table-striped table-bordered">
                <tr>
                    <th>
                        Ranking
                    </th>
                    <th>
                        Total
                    </th>
                    <th>
                        Kindergarten
                    </th>
                    <th>
                        Primary
                    </th>
                    <th>
                        Secondary
                    </th>
                    <th>
                        Highschool
                    </th>
                </tr>
                <tbody>
                    <tr>
                        <td>Ineffective</td>
                        @foreach (json_decode($chart_com1_ineff) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Developing</td>
                        @foreach (json_decode($chart_com1_devel) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Accomplishing</td>
                        @foreach (json_decode($chart_com1_accom) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Outstanding</td>
                        @foreach (json_decode($chart_com1_outst) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
            <div id="criteria_per_com_1" style="height: 300px; width: 100%;"></div>
            <hr>
            <div class="panel-heading accordion-head" style="text-align:center;">
                <h3>
                    <b>
                        Classification by Levels (Components 2+3)
                    </b>
                </h3>
            </div>
            <table id="customers" class="table table-striped table-bordered">
                <tr>
                    <th>
                        Ranking
                    </th>
                    <th>
                        Total
                    </th>
                    <th>
                        Kindergarten
                    </th>
                    <th>
                        Primary
                    </th>
                    <th>
                        Secondary
                    </th>
                    <th>
                        Highschool
                    </th>
                </tr>
                <tbody>
                    <tr>
                        <td>Ineffective</td>
                        @foreach (json_decode($chart_total_ineff) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Developing</td>
                        @foreach (json_decode($chart_total_devel) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Accomplishing</td>
                        @foreach (json_decode($chart_total_accom) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Outstanding</td>
                        @foreach (json_decode($chart_total_outst) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
            <div id="criteria_per_total" style="height: 300px; width: 100%;"></div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="panel-heading accordion-head" style="text-align:center;">
                <h3>
                    <b>
                        Classification by Levels (Components 1)
                    </b>
                </h3>
            </div>
            <table id="customers" class="table table-striped table-bordered">
                <tr>
                    <th>
                        Ranking
                    </th>
                    <th>
                        Total
                    </th>
                    <th>
                        Kindergarten
                    </th>
                    <th>
                        Primary
                    </th>
                    <th>
                        Secondary
                    </th>
                    <th>
                        Highschool
                    </th>
                </tr>
                <tbody>
                    <tr>
                        <td>Ineffective</td>
                        @foreach (json_decode($chart_com1_ineff) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Developing</td>
                        @foreach (json_decode($chart_com1_devel) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Accomplishing</td>
                        @foreach (json_decode($chart_com1_accom) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Outstanding</td>
                        @foreach (json_decode($chart_com1_outst) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
            <div id="criteria_per_com_1" style="height: 300px; width: 100%;"></div>
            <hr>
            <div class="panel-heading accordion-head" style="text-align:center;">
                <h3>
                    <b>
                        Classification by Levels (Components 2+3)
                    </b>
                </h3>
            </div>
            <table id="customers" class="table table-striped table-bordered">
                <tr>
                    <th>
                        Ranking
                    </th>
                    <th>
                        Total
                    </th>
                    <th>
                        Kindergarten
                    </th>
                    <th>
                        Primary
                    </th>
                    <th>
                        Secondary
                    </th>
                    <th>
                        Highschool
                    </th>
                </tr>
                <tbody>
                    <tr>
                        <td>Ineffective</td>
                        @foreach (json_decode($chart_total_ineff) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Developing</td>
                        @foreach (json_decode($chart_total_devel) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Accomplishing</td>
                        @foreach (json_decode($chart_total_accom) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td>Outstanding</td>
                        @foreach (json_decode($chart_total_outst) as $item)
                        <td>
                            {{$item->y}}
                        </td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
            <div id="criteria_per_total" style="height: 300px; width: 100%;"></div>
        </div>
    </div>
</div>
<hr>


@endsection
@section('script')

<script src="be/js/charts/canvas.js"></script>
@endsection