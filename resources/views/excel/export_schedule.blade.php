<table id="customers">
    <thead>
        <tr>
            <th>id</th>
            <th>grade</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($schedules as $item)
        @php
            $course = App\Course::where('course_name', $item->class)->first();
        @endphp
        <tr>
            
            <td>
                {{$item->id}}
            </td>
            <td>
                @if ($course == null)
                    0
                @else
                {{$course->grade}}
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>